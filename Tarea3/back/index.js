const express = require('express');
const cors = require('cors');

const app = express();
const PORT = 5000;

app.use(cors());
app.use(express.json());

app.get('/api', async (req, res) => {
    try {
        const respuesta = {
            nombre : "Geremias Lòpez Suruy",
            carnet : "200313184",
            curso : "LABORATORIO ANALISIS Y DISENO DE SISTEMAS 1 Sección B",
            aux_favorito : "Bryan Gerardo Páez Morales <3"
          };

        res.status(200).json(respuesta);
    } catch (error) {
        console.error(error);
        res.status(500).json({ error: 'Error al endpoint' });
    }
});

app.listen(PORT, () => {
    console.log(`Servidor corriendo en http://localhost:${PORT}`);
});
