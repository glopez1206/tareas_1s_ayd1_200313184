# Readme

![a mac](mac.jpg)


# REQUERIMIENTOS FUNCIONALEs
## Registro de Usuarios: 

### Permitir a los usuarios crear cuentas en la página de McDonald's para acceder a funcionalidades adicionales como realizar pedidos en línea y recibir ofertas personalizadas.

## Realizar Pedidos en Línea:
###  Proporcionar una interfaz intuitiva para que los usuarios puedan navegar por el menú, seleccionar productos y realizar pedidos desde la página web de McDonald's.

## Personalización de Pedidos: 
### Permitir a los usuarios personalizar sus pedidos según sus preferencias, como agregar o quitar ingredientes, elegir el tamaño de las porciones, etc.

## Pago en Línea:
###  Integrar un sistema seguro de pago en línea que permita a los usuarios realizar transacciones de manera segura y conveniente.

## Seguimiento de Pedidos: 

### Proporcionar a los usuarios la capacidad de rastrear el estado de sus pedidos en tiempo real, desde el momento en que se realiza la orden hasta que se entrega.

## Gestión de Cuentas:
### Permitir a los usuarios gestionar sus cuentas, incluyendo la edición de información personal, la revisión de historiales de pedidos y la gestión de métodos de pago.

## Localización de Restaurantes:
###  Integrar un localizador de restaurantes que permita a los usuarios encontrar fácilmente la ubicación más cercana de McDonald's en función de su ubicación.

## Ofertas y Promociones:
###  Mostrar a los usuarios ofertas y promociones especiales disponibles en su área, lo que puede incluir cupones digitales o descuentos exclusivos.

## Compatibilidad Móvil: 
### Asegurar que la página web sea completamente funcional y fácil de usar en dispositivos móviles, como smartphones y tablets.

## Feedback de Usuarios: 
### Incluir un mecanismo para que los usuarios puedan proporcionar comentarios y calificaciones sobre su experiencia de compra en línea, lo que ayuda a mejorar los servicios de McDonald's.

# Requerimientos No Funcionales: 

## Seguridad: 
### Garantizar la seguridad de la información del usuario y los datos de pago mediante el uso de protocolos de encriptación robustos y medidas de seguridad adecuadas.

## Rendimiento: 
### Optimizar el rendimiento del sitio web para garantizar tiempos de carga rápidos y una experiencia fluida de navegación, incluso durante períodos de alto tráfico.

## Disponibilidad: 
### Mantener la disponibilidad del sitio web las 24 horas del día, los 7 días de la semana, con un tiempo de inactividad mínimo planificado para mantenimiento.

## Usabilidad: 
### Diseñar la interfaz de usuario de manera intuitiva y fácil de usar, considerando la accesibilidad para personas con discapacidades y diferentes niveles de habilidad tecnológica.


## Compatibilidad del Navegador: 
### Garantizar que la página web sea compatible con una amplia gama de navegadores web populares, como Google Chrome, Mozilla Firefox, Safari, etc.

## Escalabilidad: 
### Diseñar la arquitectura del sitio web de manera que pueda escalar fácilmente para manejar un aumento en el número de usuarios y transacciones sin comprometer el rendimiento.

## Cumplimiento Normativo:
### Asegurar que el sitio web cumpla con todas las regulaciones y normativas aplicables, especialmente en lo que respecta a la privacidad de datos y la seguridad de las transacciones en línea.

## Internacionalización: 
### Adapatar la página web para que sea accesible y relevante para usuarios de diferentes regiones y culturas, incluyendo la traducción de contenido y la aceptación de diferentes monedas y métodos de pago.

## Mantenibilidad:
### Diseñar el sitio web con código limpio y bien documentado, facilitando futuras actualizaciones, mantenimiento y corrección de errores.

## Consistencia de Marca: 
### Mantener una coherencia en la experiencia de marca en línea con la imagen y los valores de McDonald's, asegurando que el diseño y el tono del sitio web reflejen la identidad de la marca.
